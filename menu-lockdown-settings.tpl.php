<div id="menu-lockdown-settings" class="clearfix">
  <div class="left">
    <?php print drupal_render($form['widget']); ?>
  </div>
  <div class="right">
    <?php print drupal_render_children($form); ?>
  </div>
</div>
